package com.techx.pax.core.util;

import org.springframework.util.StringUtils;

/**
 * Created by Carina.
 */
public class MessageUtil {

    private MessageUtil() {
    }

    public static String getLogMessage(final String message, final String uuid, final Class classz, final String operation,
                                       final String complement) {
        final StringBuilder sb = new StringBuilder();

        if (uuid != null) {
            sb.append("[").append(uuid).append("] - ");
        }
        sb.append(message);
        if (classz != null) {
            sb.append(" [").append(classz.getSimpleName()).append("]");
        }
        if (!StringUtils.isEmpty(operation)) {
            sb.append(" - ").append(operation);
        }
        if (!StringUtils.isEmpty(complement)) {
            sb.append(": ").append(complement);
        }

        return sb.toString();
    }

}