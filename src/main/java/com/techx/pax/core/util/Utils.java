package com.techx.pax.core.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {

    private Utils() {
    }

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public static Timestamp convertToTimestamp(final CharSequence date) {
        Timestamp dateTime = null;
        if (date != null) {
            final LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
            dateTime = Timestamp.valueOf(localDateTime);
        }
        return dateTime;
    }

    public static String convertFromTimestamp(final Timestamp date) {
        String dateTime = null;
        if (date != null) {
            final LocalDateTime localDateTime = date.toLocalDateTime();
            dateTime = localDateTime.format(formatter);
        }
        return dateTime;
    }

}